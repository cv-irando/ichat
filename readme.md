# IChat

Simple chat application with:

- Nodejs 
- ExpressJs
- Socket.io
- Bootstrap 4
- VueJs

By: [CV.IRANDO](https://irando.co.id) &copy; 2020.