// by CV.Irando
// www.irando.co.id

var express = require('express')
var app = express()

var http = require('http').createServer(app);
var io = require('socket.io')(http);


app.use('/js', express.static(__dirname + '/node_modules/jquery/dist')); // redirect JS jQuery
app.use(express.static(__dirname + '/node_modules/bootstrap/dist/')); // redirect bootstrap JS
app.use('/js', express.static(__dirname + '/node_modules/vue/dist')); // redirect JS vue
app.use('/js', express.static(__dirname + '/node_modules/popper.js/dist')); // redirect JS popper
app.use(express.static(__dirname + '/node_modules/bootstrap/dist')); // redirect CSS bootstrap


app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

io.on('connection', function(socket){

    socket.emit('onlines', Object.keys(io.sockets.connected).length)

    console.log('a user connected');
    socket.on('disconnect', function(){
      console.log('user disconnected');
    });

    socket.on('created', (data) => {
        socket.broadcast.emit('created', (data));
    });
    
    socket.on('chat-message', (data) => {
        socket.broadcast.emit('chat-message', (data));
    });

    socket.on('typing', (data) => {
        socket.broadcast.emit('typing', (data));
    });

    socket.on('stopTyping', (data) => {
        socket.broadcast.emit('stopTyping', (data));
    });

    socket.on('joined', (data) => {
        socket.broadcast.emit('joined', (data));
    });

    socket.on('leaved', (data) => {
        socket.broadcast.emit('leaved', (data));
    });
});

http.listen(3000, function(){
  console.log('listening on *:3000');
});